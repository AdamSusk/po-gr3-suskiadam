package pl.edu.uwm.wmii.suskiadam.laboratorium00;
import java.util.Scanner;

public class Lab4zadanie1c {
    public static String middle(String str){
        int rozmiar=0;
        int pozycja=0;
        if(str.length()%2 ==0){
            pozycja = str.length()/2-1;
            rozmiar = 2;
        }
        else{
            pozycja = str.length()/2;
            rozmiar =1;
        }
        return str.substring(pozycja,rozmiar+pozycja);
    }


    public static void main(String[] args) {
        Scanner x = new Scanner(System.in);
        String str = x.nextLine();
        System.out.println(middle(str));



    }
}
