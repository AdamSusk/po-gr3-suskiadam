package pl.edu.uwm.wmii.suskiadam.laboratorium00;
import java.util.Scanner;
import java.util.Random;
public class Laboratorium02zadanie1d {

    public void zadanied(int n) {
        int dodatnie = 0;
        int ujemne = 0;
        int tab[] = new int[n];
        Random rand = new Random();
        if (1 <= n && n <= 100) {
            for (int i = 0; i < n; i++) {
                int losowe = rand.nextInt(999 + 999 - 1) - 999;
                tab[i] = losowe;
                System.out.println(tab[i]);
            }
        }
        for (int i = 0; i < n; i++) {
            if (tab[i] >= 0){
                dodatnie = tab[i]+dodatnie;
            }
            if(tab[i]<0){
                ujemne = tab[i]+ujemne;
            }
        }
        System.out.println("Suma liczb dodatnich "+dodatnie);
        System.out.println("Suma liczb ujemnych "+ujemne);
    }

    public static void main(String[] args) {
        Laboratorium02zadanie1d x = new Laboratorium02zadanie1d();
        Scanner wczytaj = new Scanner(System.in);
        int n = wczytaj.nextInt();
        x.zadanied(n);


    }
}
