package pl.edu.uwm.wmii.suskiadam.laboratorium00;

import java.util.Scanner;
import java.lang.*;
public class Laboratorium01zadanie1 {
    public void zadaniea(int liczba){
        int wynik=0;
        for(int i=0;i<liczba;i++){
            Scanner x = new Scanner(System.in);
            int suma = x.nextInt();
            wynik += suma;
        }
        System.out.println(wynik);
    }
    public void zadanieb(int liczba){
        int wynik=0;
        for(int i=0;i<liczba;i++){
            Scanner x = new Scanner(System.in);
            int iloczyn = x.nextInt();
            if(i==0){
                wynik += iloczyn;
            }
            else {
                wynik *= iloczyn;
            }
        }
        System.out.println(wynik);
    }

    public void zadaniee(int liczba){
        int wynik=0;
        for(int i=0;i<liczba;i++){
            Scanner x = new Scanner(System.in);
            int iloczyn = x.nextInt();
            if(i==0){
                wynik += Math.abs(iloczyn);
            }
            else {
                wynik *= Math.abs(iloczyn);
            }
        }
        System.out.println(wynik);
    }

    public void zadanieg(int liczba) {
        int wynikSumy = 0;
        int wynikIloczynu =0;
        for (int i = 0; i < liczba; i++) {
            Scanner x = new Scanner(System.in);
            int suma = x.nextInt();
            int iloczyn = suma;
            wynikSumy += suma;
            if (i == 0) {
                wynikIloczynu += iloczyn;
            }
            else{
                wynikIloczynu *= iloczyn;
            }
        }
        System.out.println("Suma: "+ wynikSumy);
        System.out.println("Iloczyn: "+ wynikIloczynu);
    }
    public void zadanieh(int liczba){
        int wynik=0;
        for(int i=1;i<=liczba;i++){    // zaczyna od 1 dlatego bo pierwsza liczba ma być dodatnia
            Scanner x = new Scanner(System.in);
            int suma = x.nextInt();
            wynik += suma*Math.pow(-1,i+1);
        }
        System.out.println(wynik);
    }
    public long silnia(int a){
        if(1>a){
            return 1;
        }
        else{
            return a*silnia(a-1);
        }
    }
    public void zadaniei(int liczba){
        float wynik=0;
        for(int i=1;i<=liczba;i++){
            Scanner x = new Scanner(System.in);
            int suma = x.nextInt();
            wynik += (suma/silnia(i))*Math.pow(-1,i);
        }
        System.out.println(wynik);
    }
    public static void main(String[] args) {
        Laboratorium01zadanie1 z = new Laboratorium01zadanie1();
        Scanner wczytaj = new Scanner(System.in);
        int liczba = wczytaj.nextInt();
        z.zadaniea(liczba);


    }
}
