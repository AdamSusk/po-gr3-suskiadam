package pl.edu.uwm.wmii.suskiadam.laboratorium00;

import java.lang.*;
import java.util.Scanner;
public class Laboratorium01zadanie2 {
    public void Zadaniea(int liczba){
        int wynik = 0;
        for(int i=0;i<liczba;i++){
            Scanner x = new Scanner(System.in);
            int wartosc = x.nextInt();
            if(wartosc % 2 !=0){
                wynik++;
            }
        }
        System.out.println("Nieparzystych liczb jest: "+wynik);
    }
    public void Zadanieb(int liczba){
        int wynik =0;
        for(int i=0;i<liczba;i++){
            Scanner x = new Scanner(System.in);
            int wartosc = x.nextInt();
            if((wartosc % 3 ==0)&&(wartosc % 5 !=0)){
                wynik++;
            }
        }
        System.out.println("Liczb podzielnych przez 3 i niepodzielnych przez 5 jest: "+wynik);
    }

    public void Zadanied(int liczba){
        int tab[] = new int[liczba];
        int wynik =0;
        for(int i=0;i<liczba;i++){
            Scanner x = new Scanner(System.in);
            int wartosc = x.nextInt();
            tab[i]=wartosc;
        }
        for(int i=0;i<liczba;i++){
            if((i>0)&&(i<liczba-1)&&(tab[i]<(tab[i-1]+tab[i+1])/2))
            {
                wynik++;
            }
        }
        System.out.println("Liczb spelniajacych warunki jest"+ wynik);
    }

    public void Zadanief(int liczba){
        int wynik =0;
        for(int i=1;i<=liczba;i++){ // i=1 numerujemy od 1 do n
            Scanner x =new Scanner(System.in);
            int wartosc = x.nextInt();
            if(i%2 !=0 && wartosc % 2 ==0){
                wynik++;
            }
        }
        System.out.println("Liczb parzystych majacych nieparzysty numer jest:"+wynik);
    }


    public static void main(String[] args) {
        Laboratorium01zadanie2 c = new Laboratorium01zadanie2();
        Scanner wczytaj = new Scanner(System.in);
        int liczba = wczytaj.nextInt();
        c.Zadanief(liczba);
    }
}
