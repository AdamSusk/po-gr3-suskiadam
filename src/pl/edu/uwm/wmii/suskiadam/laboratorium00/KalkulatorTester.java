package pl.edu.uwm.wmii.suskiadam.laboratorium00;
import java.util.Scanner;

public class KalkulatorTester {
    public static void main(String[] args) {
        Kalkulator k = new Kalkulator();
        Scanner num = new Scanner(System.in);
        for(;;) {      // nieskończona pętla, bedzie dzialac dopoki uzytkownik nie wcisnie 7
            System.out.println(" ");
            System.out.print("Podaj pierwsza liczbę: ");
            String pliczba = num.nextLine();                // pierwsza liczba
            System.out.print("Podaj druga liczbę: ");
            String dliczba = num.nextLine();                // druga liczba
            k.setLiczby(pliczba, dliczba);         //przekazanie liczb przez argumenty do parametrów
            k.menu();
            System.out.print("Wybierz: ");
            String wybrana = num.nextLine();    // wybranie działania
            k.obliczanie(wybrana);      // przekazanie wybranego działania
        }
    }
}
