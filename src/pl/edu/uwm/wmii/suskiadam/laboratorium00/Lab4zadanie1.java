package pl.edu.uwm.wmii.suskiadam.laboratorium00;
import java.util.Scanner;

public class Lab4zadanie1 {
    public static int countChar(String str,char c){
        int licznik=0;
        char znak;
        for(int i=0;i<str.length();i++){
            znak = str.charAt(i);
            if(znak == c){
                licznik++;
            }
        }
        return licznik;
    }

    public static void main(String[] args) {
        System.out.println(countChar("Internet",'e'));


    }
}
