package pl.edu.uwm.wmii.suskiadam.laboratorium00;
import static java.lang.Math.*;

public class Kalkulator {
    public void menu() {
        System.out.println(" ");
        System.out.println("MENU GLOWNE");
        System.out.println("-----------");
        System.out.println("1. Dodawanie");
        System.out.println("2. Odejmowanie");
        System.out.println("3. Mnożenie");
        System.out.println("4. Dzielenie");
        System.out.println("5. Modulo");
        System.out.println("6.Pierwiastek");
        System.out.println("7.Wyjście");
    }


    int a, b;

    public void setLiczby(String liczba1, String liczba2) {
        String x = liczba1;
        String y = liczba2;
        a = Integer.parseInt(x);
        b = Integer.parseInt(y);
    }

    public void obliczanie(String wybor) {
        int wyborNum = Integer.parseInt(wybor);   //argument był zmienną String, więc zmieniamy ją
        //na zmienną typu int
        int c;
        switch(wyborNum)
        {
            case 1: c = a + b;
                System.out.println("Suma: " + c );
                break;

            case 2: c = a - b;
                System.out.println("Różnica: " + c);
                break;

            case 3: c = a * b;
                System.out.println("Iloczyn: " + c);
                break;

            case 4: if (b==0) {
                System.out.println("Nie dzielimy przez zero!");
                break;
            } else {
                c = a / b;
                System.out.println("Wynik: " + c);
                break;
            }
            case 5: c=a%b;
                System.out.println("Dzielenie modulo"+c);
            default: System.exit(0);
        }
    }
}
