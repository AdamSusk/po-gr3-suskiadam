package pl.edu.uwm.wmii.suskiadam.laboratorium00;
import java.util.ArrayList;

public class IntegerSet {
    boolean[]  liczby = new boolean[100];



    static boolean[] intersection(boolean[] a, boolean[] b){
        boolean[] wynik = new boolean[100];
        for(int i = 0; i < 100; i++)
        {
            if(a[i] == true && b[i] == true)
                wynik[i] = true;
        }
        return wynik;
    }

    static boolean[] union(boolean[] a, boolean[] b){
        boolean[] wynik = new boolean[100];
        for(int i = 0; i < 100; i++)
        {
            if(a[i] == true || b[i] == true)
                wynik[i] = true;
        }
        return wynik;
    }

    void deleteElement(int element){
        if(element > 0 && element < 101)
            liczby[element-1] = false;
        else
            System.out.println("Liczba nie miesci sie w zakresie <1, 100>");
    }

    void insertElement(int element){
        if(element > 0 && element < 101)
            liczby[element-1] = true;
        else
            System.out.println("Liczba nie miesci sie w zakresie <1, 100>");
    }



    String toStr (){
        ArrayList<Integer> Array = new ArrayList<>();
        for(int i = 0; i < 100; i++)
        {
            if(liczby[i] == true)
                Array.add(i+1);
        }
        return Array.toString();
    }

    boolean equals(boolean[] a){
        for(int i = 0; i < 100; i++)
        {
            if(liczby[i] != a[i])
                return false;
        }
        return true;
    }
}