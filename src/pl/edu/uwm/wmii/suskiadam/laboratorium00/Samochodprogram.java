package pl.edu.uwm.wmii.suskiadam.laboratorium00;
import java.util.*;

public class Samochodprogram {

    public static void main(String[] args) {
        samochod pierwszy = new samochod();
        pierwszy.marka = "Ford";
        pierwszy.rocznik = 1998;
        pierwszy.pojSilnika = 1.6F;

        samochod drugi = new samochod();
        drugi.marka = "Renault";
        drugi.rocznik = 2005;
        drugi.pojSilnika = 1.4F;

        samochod trzeci = new samochod();
        trzeci.marka = "Opel";
        trzeci.rocznik = 1968;
        trzeci.pojSilnika = 2.8F;

        pierwszy.obliczSpalanieNaTrasie(100, pierwszy.pojSilnika);
        System.out.println(pierwszy.SpalanienaTrasie);
        drugi.obliczSpalanieNaTrasie(100, drugi.pojSilnika);
        System.out.println(drugi.SpalanienaTrasie);
        trzeci.obliczSpalanieNaTrasie(100, trzeci.pojSilnika);
        System.out.println(trzeci.SpalanienaTrasie);

        ArrayList<samochod> lista = new ArrayList<samochod>();
        lista.add(pierwszy);
        lista.add(drugi);
        lista.add(trzeci);

        for (samochod om : lista)
        {
            om.wypisz();
        }
    }
}
