package pl.edu.uwm.wmii.suskiadam.laboratorium00;

public class Zadanie11 {public static void main(String[] args) {
    System.out.println("Julian Tuwim \"Do prostego człowieka\"");
    System.out.println(" ");
    System.out.println("Gdy znów do murów klajstrem świeżym");
    System.out.println("Przylepiać zaczną obwieszczenia,");
    System.out.println("Gdy \"do ludności\", \"do żołnierzy\"");
    System.out.println("Na alarm czarny druk uderzy");
    System.out.println("I byle drab, i byle szczeniak");
    System.out.println("W odwieczne kłamstwo ich uwierzy,");
    System.out.println("Że trzeba iść i z armat walić,");
    System.out.println("Mordować, grabić, truć i palić;");
    System.out.println("Gdy zaczną na tysięczną modłę");
    System.out.println("Ojczyznę szarpać deklinacją");
    System.out.println("I łudzić kolorowym godłem,");
    System.out.println("I judzić \"historyczną racją\",");
    System.out.println("O piędzi, chwale i rubieży,");
    System.out.println("O ojcach, dziadach i sztandarach,");
    System.out.println("O bohaterach i ofiarach;");
    System.out.println("Gdy wyjdzie biskup, pastor, rabin");
    System.out.println("Pobłogosławić twój karabin,");
    System.out.println("Bo mu sam Pan Bóg szepnął z nieba,");
    System.out.println("Że za ojczyznę - bić się trzeba;");




}
}
