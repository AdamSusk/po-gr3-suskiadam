package pl.edu.uwm.wmii.suskiadam.laboratorium00;

public class Lucznik {
    String imie = "OrkA";
    private double zywotnosc = 100.0D;
    private int zrecznosc = 15;
    int PT = 3;
    double moc_ataku;

    public Lucznik() {
    }

    public void zywotnosc(double x) {
        this.zywotnosc = x;
        if (0.0D > x) {
            this.zywotnosc = 0.0D;
        }

        if (100.0D < x) {
            this.zywotnosc = 100.0D;
        }

    }

    public void moc_ataku() {
        this.moc_ataku = (double)(this.zrecznosc * this.PT) * this.zywotnosc;
    }

    public String toString() {
        return " imie: " + this.imie + "\n życie: " + this.zywotnosc+ "\n sila: " + this.zrecznosc + "\n PT: " + this.PT + "\n moc ataku: " + this.moc_ataku;
    }
}