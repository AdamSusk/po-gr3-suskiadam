package pl.edu.uwm.wmii.suskiadam.laboratorium00;
import java.util.Scanner;
public class TesterRachunek {
    public static void main(String[] args) {
        RachunekBankowy saver1 = new RachunekBankowy(2000);
        RachunekBankowy saver2=new RachunekBankowy(3000);
        saver1.setRocznaStopaProcentowa(4);
        saver2.setRocznaStopaProcentowa(4);
        saver1.ObliczMiesieczneOdsetki();
        saver2.ObliczMiesieczneOdsetki();
        System.out.println("Saldo po pierszym miesiącu:");
        System.out.print("Saver1: ");
        saver1.wyswietlSaldo();
        System.out.print("Saver2: ");
        saver2.wyswietlSaldo();
        saver1.setRocznaStopaProcentowa(5);
        saver2.setRocznaStopaProcentowa(5);
        saver1.ObliczMiesieczneOdsetki();
        saver2.ObliczMiesieczneOdsetki();
        System.out.println("Saldo po drugim miesiącu:");
        System.out.print("Saver1: ");
        saver1.wyswietlSaldo();
        System.out.print("Saver2: ");
        saver2.wyswietlSaldo();
    }
}
