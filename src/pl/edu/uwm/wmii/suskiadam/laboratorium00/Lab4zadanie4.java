package pl.edu.uwm.wmii.suskiadam.laboratorium00;

import java.math.BigInteger;
import java.util.*;
public class Lab4zadanie4 {

    public  void zadanie(int n) {
        BigInteger[][] szachownica = new BigInteger[n][n];
        BigInteger ziarna = new BigInteger("2");
        int potega=0;
        BigInteger suma = new BigInteger("0");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                BigInteger wynik = ziarna.pow(potega);
                szachownica[i][j] = wynik;
                potega++;
                suma = suma.add(wynik);
            }
        }
        System.out.println(Arrays.deepToString(szachownica));
        System.out.println("Suma: "+suma);
    }

    public static void main(String[] args) {
        Lab4zadanie4 szachy = new Lab4zadanie4();
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        szachy.zadanie(n);
    }
}