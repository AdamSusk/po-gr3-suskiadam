package pl.edu.uwm.wmii.suskiadam.laboratorium00;
import java.util.Scanner;
import java.util.Random;
public class Laboratorium02zadanie1f {

    public void zadanief(int n){
        int tab[] = new int[n];
        int naj[] = new int[n];
        Random rand = new Random();
        int wynik =0;
        int dlugosc =0;
        if(1 <=n && n <= 100) {
            for (int i = 0; i < n; i++) {  // zapelniamy tablice zerami
                naj[i] = 0;
            }
            for (int i = 0; i < n; i++) { // losujemy liczby z przedzialu -999-999
                int losowe = rand.nextInt(999 + 999 - 1) - 999;
                tab[i] = losowe;
                System.out.println(tab[i]);
            }
        }
        for(int i=0;i<n;i++) { // sumujemu ilosc liczb dodatnich
            if(tab[i]>=0){
                wynik++;
            }
            else{
                naj[i] = wynik; // wprowadzanie wyniku do tablicy
                wynik =0;
            }
        }
        for(int i=0;i<n;i++){
            if(naj[i]>dlugosc){ // szukanie najluzszego elementu
                dlugosc = naj[i];
            }
        }
        System.out.println("Najdluzszy fragment tablicy skadajacy sie z liczb dodatnich ma dlugosc: " + dlugosc);
    }



    public static void main(String[] args) {
        Laboratorium02zadanie1f x = new Laboratorium02zadanie1f();
        Scanner wczytaj = new Scanner(System.in);
        int n = wczytaj.nextInt();
        x.zadanief(n);


    }
}