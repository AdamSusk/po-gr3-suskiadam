package pl.edu.uwm.wmii.suskiadam.laboratorium00;

import java.util.Random;
import java.util.Scanner;

public class Laboratorium02zadanie1a {

    public void zadanie1(int n){
        int parzyste=0;
        int nieparzyste =0;
        int tab[] = new int[n];
        Random rand = new Random();
        if(n>=1&& n <=100) {
            for (int i = 0; i < n; i++) {
                int losowe = rand.nextInt(999 + 999 + 1) - 999;
                tab[i] = losowe;
                System.out.println(tab[i]);
            }
        }
        for(int i=0;i<n;i++){
            if(tab[i] % 2 == 0){
                parzyste++;
            }
            if(tab[i] % 2 != 0 ){
                nieparzyste++;
            }
        }
        System.out.println("W tablicy znajduje sie: "+parzyste +"elementow parzystych, oraz:"+nieparzyste +"elementow nieparzystych");
    }


    public static void main(String[] args) {
        Laboratorium02zadanie1a x = new Laboratorium02zadanie1a();
        Scanner wczytaj = new Scanner(System.in);
        int n = wczytaj.nextInt();
        x.zadanie1(n);


    }
}
