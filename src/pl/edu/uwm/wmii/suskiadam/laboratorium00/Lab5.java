package pl.edu.uwm.wmii.suskiadam.laboratorium00;
import java.util.ArrayList;
import java.util.Collections;



public class Lab5 {
    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b)
    {
        ArrayList<Integer> laczona = new ArrayList<>();
        laczona.addAll(a);
        laczona.addAll(b);
        return laczona;
    }

    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b)
    {
        ArrayList<Integer> wynik = new ArrayList<>();
        int dlugosc1 = a.size();
        int dlugosc2 = b.size();
        for(int i = 0; i<Math.min(dlugosc1,dlugosc2);i++) {
            wynik.add(a.get(i));
            wynik.add(b.get(i));
        }
        if(dlugosc1>dlugosc2)
        {
            for(int i = dlugosc2; i<dlugosc1;i++) {
                wynik.add(a.get(i));
            }
        }
        else if(dlugosc2>dlugosc1)
        {
            for(int i = dlugosc1; i<dlugosc2;i++) {
                wynik.add(b.get(i));
            }
        }
        return wynik;
    }

    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b) {

        int dlugosc1 = a.size();
        int dlugosc2 = b.size();
        Collections.sort(a);
        Collections.sort(b);
        ArrayList<Integer> sortowana = new ArrayList<>();
        int i = 0, j = 0, k = 0;

        while (i < dlugosc1 && j < dlugosc2) {

            if (a.get(i) < b.get(j))
                sortowana.add(k++,a.get(i++));
            else
                sortowana.add(k++,b.get(j++));
        }

        while (i < dlugosc1) {
            sortowana.add(k++, a.get(i++));
        }

        while (j < dlugosc2) {
            sortowana.add(k++, b.get(j++));
        }
        return sortowana;
    }

    public static ArrayList<Integer> reversed(ArrayList<Integer> a) {
        ArrayList<Integer> odwrocona = new ArrayList<>();
        int rozmiar = a.size();
        for(int i=rozmiar-1;i>=0;i--) {
            odwrocona.add(a.get(i));
        }
        return odwrocona;
    }

    public static void reverse(ArrayList<Integer> a) {
        ArrayList<Integer> lista = new ArrayList<>();
        int rozmiarA = a.size();
        for(int i=rozmiarA-1;i>=0;i--) {
            lista.add(a.get(i));
        }
        int rozmiar = lista.size();
        a.removeAll(a);
        for(int i=0;i<rozmiar;i++) {
            a.add(lista.get(i));
        }
    }

    public static void main(String[] args) {
        ArrayList<Integer> pierwszy =new ArrayList<>();
        ArrayList<Integer> drugi =new ArrayList<>();
        pierwszy.add(0,1);
        pierwszy.add(1,4);
        pierwszy.add(2,9);
        pierwszy.add(3,16);
        drugi.add(0,9);
        drugi.add(1,7);
        drugi.add(2,4);
        drugi.add(3,9);
        drugi.add(4,11);
        System.out.println(append(pierwszy,drugi));
        System.out.println(merge(pierwszy,drugi));
        System.out.println(mergeSorted(pierwszy,drugi));
        System.out.println(reversed(pierwszy));
        reverse(pierwszy);
        System.out.println(pierwszy);

    }

}
