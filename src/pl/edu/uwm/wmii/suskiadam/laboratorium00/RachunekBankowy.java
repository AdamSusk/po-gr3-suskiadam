package pl.edu.uwm.wmii.suskiadam.laboratorium00;

public class RachunekBankowy {
    static double rocznaStopaProcentowa;
    private double saldo;

    public RachunekBankowy(){
        rocznaStopaProcentowa = 0.01;
        saldo = 1000;
    }

    public RachunekBankowy(double startSaldo){
        rocznaStopaProcentowa = 0.01;
        saldo = startSaldo;
    }

    public RachunekBankowy(double startRocznaStopa, double startSaldo){
        rocznaStopaProcentowa = startRocznaStopa;
        saldo = startSaldo;
    }


    void ObliczMiesieczneOdsetki(){
        double odsetki = 0;
        odsetki = (saldo * rocznaStopaProcentowa)/12;
        saldo = saldo + odsetki;
    }

    static void setRocznaStopaProcentowa(double stopaProcentowa){
        rocznaStopaProcentowa = stopaProcentowa / 100;
    }

    void wyswietlSaldo(){
        System.out.println(saldo);
    }

}
